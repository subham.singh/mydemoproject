import React from 'react';


/**
 * This is the functional component
 */
// function Greet() {
//     return <h1>Hello Subham</h1>
// }


export const Greet = (props) => {
    console.log(props);
    // immutable property of props
    // props.name='Subham';
    return (
        <div>
            <h1>Hello {props.name} a.k.a {props.heroName}</h1>
            {props.children}
        </div>
    )
}

// export default Greet;