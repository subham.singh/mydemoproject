import React from 'react';
// import logo from './logo.svg';
import './App.css';

// import Greet from './components/Greet';
import { Greet } from './components/Greet';
import Welcome from './components/Welcome';
// import Hello from './components/Hello';

import Message from './components/message';

function App() {
  return (
    <div className="App">
      <Message/>
      {/* <Greet name="Dev" heroName="Batman">
        <p>This is children props</p>
      </Greet>
      <Greet name="Brom" heroName="Superman">
        <button>Action Button</button>
      </Greet>
      <Greet name="Herom" heroName="Ironman" /> */}
      {/* <Welcome name="Dev" heroName="Batsman" />
      <Welcome name="Brom" heroName="Superman" />
      <Welcome name="Herom" heroName="Ironman" /> */}
      {/* <Hello /> */}
    </div>
  );
}

export default App;
